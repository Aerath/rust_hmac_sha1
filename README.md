# Rust-HMAC-SHA1

[![Build Status](https://travis-ci.org/aerath/rust_hmac_sha1.png?branch=master)](https://github.com/aerath/rust_hmac_sha1)
![creates.io version](https://img.shields.io/crates/v/hmac_sha1.svg)

A pure rust implementation of the Hash-based Message Authentication Code Algoritm for SHA1.

## Usage

To import rust_hmac_sha1 add the following to your Cargo.toml:
```toml
[dependencies]
hmac_sha1 = "^0.2"
```

To use rust_hmac_sha1 add the following to your crate root:
```rust
extern crate hmac_sha1;
```
## Contributions

Any contributions are welcome. This was implemented as a learning experience and any advice is appreciated.

## License

This crate is licensed under the BSD 3-Clause license, as is its dependancy [sha1](https://github.com/mitsuhiko/rust-sha1)
